//Adam Atamnia 2036819

'use strict';
/** 
 * JS Project 1
 * Spending Log 
 * 
 * @author Adam Atamnia
 * 2021-10-12
 */
document.addEventListener('DOMContentLoaded', setup)

let spendingEntries = []

/**
 * First funtion that is called after the event (DOMContentLoaded)
 */
function setup() {
    loadSpending()
    let form = document.querySelector('.spending-log-form')
    form.addEventListener('submit', submit)
}

/**
 * Creates a new entry JSON object, and add it to global spending entry array. 
 * Creates and appends a new entry in the appropriate section of the html.
 * @param {object} e event it was called by
 */
function submit(e) {
    e.preventDefault()

    let form = document.querySelector('.spending-log-form')
    let dateValue = document.querySelector('#date').value
    let descriptionValue = document.querySelector('#description').value
    let amountValue = document.querySelector('#amount').value
    let categoryValue = Array.from(document.getElementsByName('category')).find(value => value.checked).value
    let entry = {
        date: dateValue,
        description: descriptionValue,
        amount: amountValue,
        category: categoryValue,
        id: spendingEntries.length
    }
    spendingEntries.push(entry)
    writeSpending()
    console.log(spendingEntries)
    addEntryToDOM(entry)
    form.reset()
}

/**
 * Creates and appends a new entry in the appropriate section of the html.
 * @param {object} entry Entry object 
 */
function addEntryToDOM(entry) {
    let credits = document.querySelector('.credits')
    let div = document.createElement('div')
    div.classList.add('entry', entry.category + "-entry", entry.id)
    credits.parentElement.insertBefore(div, credits)

    let ul = document.createElement('ul')
    ul.classList.add('container')
    div.appendChild(ul)
    // first li
    let li1 = document.createElement('li')
    li1.classList.add('row')
    ul.appendChild(li1)

    let h2 = document.createElement('h2')
    h2.classList.add('entry-category')
    h2.textContent = entry.category
    li1.appendChild(h2)

    let amount = document.createElement('span')
    amount.classList.add('amount')
    amount.textContent = entry.amount + '$'
    li1.appendChild(amount)

    let stars = document.createElement('span')
    stars.classList.add('stars')
    let starsText = ''
    if (entry.amount <= 100) {
        starsText = '&#x26AA &#x26AA &#x26AA'
    } else if (entry.amount > 100 && entry.amount <= 500) {
        starsText = '&#x2B50 &#x26AA &#x26AA'
    } else if (entry.amount > 500 && entry.amount <= 750) {
        starsText = '&#x2B50 &#x2B50 &#x26AA'
    } else if (entry.amount > 750) {
        starsText = '&#x2B50 &#x2B50 &#x2B50'
    }
    stars.innerHTML = starsText
    li1.appendChild(stars)

    // second li
    let li2 = document.createElement('li')
    li2.classList.add('description')
    li2.textContent = entry.description
    ul.appendChild(li2)

    // third li
    let li3 = document.createElement('li')
    li3.classList.add('row')
    ul.appendChild(li3)

    let date = document.createElement('span')
    date.classList.add('date')
    date.textContent = entry.date
    li3.appendChild(date)

    let deleteBtn = document.createElement('button')
    deleteBtn.classList.add('delete-entry')
    deleteBtn.textContent = 'Delete entry'
    deleteBtn.addEventListener('click', checkedAction)
    li3.appendChild(deleteBtn)
}

/**
 * Removes entry from the spending array and the DOM when delete button is clicked.
 * @param {object} e event it was called by
 */
function checkedAction(e) {
    let entry = e.target.parentElement.parentElement.parentElement
    let id = entry.classList[entry.classList.length - 1]
    spendingEntries.splice(id, 1)
    spendingEntries.forEach((value, i) => value.id = i)

    while (entry.firstChild) {
        entry.lastChild.remove()
    }
    entry.remove()
    let entries = document.querySelectorAll('.entry')
    entries.forEach((value, i) => {
        value.classList.remove(value.classList[entry.classList.length - 1])
        value.classList.add(i)
    })

    writeSpending()
    console.log(spendingEntries)

}

/**
 * Reads from local storage, initializes the spending array and adds the entries to the DOM
 */
function loadSpending() {
    if (localStorage.getItem('spendingEntries')) {
        spendingEntries = JSON.parse(localStorage.getItem('spendingEntries'))
        spendingEntries.forEach(value => addEntryToDOM(value))
    }
}

/**
 * Writes the spending array to local storage
 */
function writeSpending() {
    localStorage.setItem('spendingEntries', JSON.stringify(spendingEntries))
}

